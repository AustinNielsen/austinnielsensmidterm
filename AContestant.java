/**
 * You need to change this file.
 * 
 * A contestant is an abstract class that represents shared functionality among all contestants
 * AContestant should inherit from IContestant
 * 
 * A contestant should have a private member called name of type String.
 * 
 * A contestant should have a constructor with one parameter of type String.
 * The constructor should assign the value of member name to the value of that parameter
 *
 * This class should override toString(). It should simply return the value of the member name.
 * 
 * @author bricks
 *
 */
public abstract class AContestant implements IContestant{

	//declare string variable
	private String name;
	
	/** Description of AContestant()
	 * 
	 * 
	 * @param inName	Name of contestant
	 */
	public AContestant(String inName) {
		this.name = inName;
	}
	
	/** Description of toString
	 * 
	 * returns name of contestant
	 */
	public String toString() {
		return name;
	}

}
