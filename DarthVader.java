/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Darth Vader.
 * 
 * When defending against Michelangelo, this class should return HelperStrings.DarthVaderDefendingMichelangelo
 * 
 * When defending against Chuck Norris, this class should return HelperStrings.DarthVaderDefendingChuckNorris
 * @author bricks
 *
 */
public class DarthVader extends AContestant {

	/** Description of DarthVader()
	 * 
	 * calls super class and sets name as 'Darth Vader'
	 */
	public DarthVader() {
		super("Darth Vader");
	}

	/**	Description of defendAgainst()
	 * 
	 * @param player	name of player of type IContestant
	 * 
	 */
	@Override
	public String defendAgainst(IContestant player) {
		
		/**
		 * if name of player is instance of Michelangelo, return DarthVader vs Michelangelo
		 * if name of player is instance of ChuckNorris, return DarthVader vs ChuckNorris
		 */
		if(player instanceof MichelangeloTheNinjaTurtle) {
			return HelperStrings.DarthVaderDefendingMichelangelo;
		}
		
		if(player instanceof ChuckNorris) {
			return HelperStrings.DarthVaderDefendingChuckNorris;
		}
		return null;
	}
	
}
