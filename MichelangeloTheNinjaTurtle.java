/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Michelangelo.
 * 
 * When defending against Chuck Norris, this class should return HelperStrings.MichelangeloDefendingChuck
 * 
 * When defending against Darth Vader, this class should return HelperStrings.MichelangeloDefendingDarthVader
 * @author bricks
 *
 */
public class MichelangeloTheNinjaTurtle extends AContestant{
	
	/** Description of MichelangeloTheNinjaTurtle()
	 * 
	 * calls super class and sets name as 'Michelangelo'
	 */
	public MichelangeloTheNinjaTurtle() {
		super("Michelangelo");
	}

	/**	Description of defendAgainst()
	 * 
	 * @param player	name of player of type IContestant
	 * 
	 */
	@Override
	public String defendAgainst(IContestant player) {
		
		/**
		 * if name of player is instance of ChuckNorris, return Michelangelo vs ChuckNorris
		 * if name of player is instance of DarthVader, return Michelangelo vs DarthVader
		 */
		if(player instanceof ChuckNorris) {
			return HelperStrings.MichelangeloDefendingChuck;
		}
		
		if(player instanceof DarthVader) {
			return HelperStrings.MichelangeloDefendingDarthVader;
		}
		return null;
	}
	
}
