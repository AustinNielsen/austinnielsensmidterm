/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Chuck Norris.
 * 
 * When defending against Michelangelo, this class should return HelperStrings.ChuckDefendingMichelangelo
 * 
 * When defending against Darth Vader, this class should return HelperStrings.ChuckDefendingDarthVader
 * @author bricks
 *
 */
public class ChuckNorris extends AContestant{

	/** Description of ChuckNorric()
	 * 
	 * calls super class and sets name as 'Chuck Norris'
	 */
	public ChuckNorris() {
		super("Chuck Norris");
	}

	/**	Description of defendAgainst()
	 * 
	 * @param player	name of player of type IContestant
	 * 
	 */
	@Override
	public String defendAgainst(IContestant player) {
		
		/**
		 * if name of player is instance of Michelangelo, return chuck vs Michelangelo
		 * if name of player is instance of DarthVader, return chuck vs DarthVader
		 */
		if(player instanceof MichelangeloTheNinjaTurtle) {
			return HelperStrings.ChuckDefendingMichelangelo;
		}
		
		if(player instanceof DarthVader) {
			return HelperStrings.ChuckDefendingDarthVader;
		}
		return null;
	}
	


}
